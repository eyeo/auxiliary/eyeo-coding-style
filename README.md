## eyeo-coding-style

This repository is a collection of styling / linting configurations and plugins
according to [Adblock Plus'](https://adblockplus.org/) conventional
[coding-style](https://adblockplus.org/coding-style).

Please refer to the subprojects for more information:
 * JavaScript: [eslint-config-eyeo](eslint-config-eyeo/README.md)
 * HTML / CSS: [stylelint-config-eyeo](stylelint-config-eyeo/README.md)
 * Python: [flake8-eyeo](flake8-eyeo/README.md)
